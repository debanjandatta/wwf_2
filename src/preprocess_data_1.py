import pandas as pd
import sys
import os


def clean_data( ):
	df = pd.read_excel('../data/WWF_2007-2013_original.xlsx')
	reqd_columns = [
		'Date Range',
		'Harmonized Tariff Schedule',
		'Carrier',
		'Source Country',
		'Lading Port',
		'Discharge Port',
		'Weight',
		'estimatedValue',
		'portArrivalOrig',
		'portDepartureOrig'
	]
	# Select columns
	df = df[reqd_columns]

	def set_month(row):
		ym = row['Date Range']
		y = int(ym / 100)
		m = ym % 100
		return m

	def set_year(row):
		ym = row['Date Range']
		y = int(ym / 100)
		return y

	df['year'] = -1
	df['month'] = -1
	df['year'] = df.apply(set_year, axis=1)
	df['month'] = df.apply(set_month, axis=1)
	del df['Date Range']

	df = df.rename(columns={
		'Harmonized Tariff Schedule': 'hs_code',
		'Carrier': 'carrier',
		'Source Country': 'source_country',
		'estimatedValue': 'value',
		'Weight': 'weight',
		'Lading Port': 'port_lading',
		'Discharge Port': 'port_discharge',
		'portDepartureOrig': 'port_departure',
		'portArrivalOrig': 'port_arrival'
	}
	)

	def filter_hs_codes(df):
		def get4digits(row):
			try:
				hsc = str(int(row['hs_code']))
				hsc = hsc[0:4]
			except:
				hsc = None
			return hsc

		df['hs_code'] = df.apply(get4digits, axis=1)
		df.dropna(subset=['hs_code'])
		return df

	# convert hs_code to 4 digits!
	df = filter_hs_codes(df)
	print('Rows in df ', len(df))
	# --------------- #

	arrival_ports_df_file = './../data/arrival_port.csv'
	if os.path.isfile(arrival_ports_df_file) == False:
		# Convert port_arrival to ids [ 0 ... n-1]
		arrival_ports = list(sorted(set(list(df['port_arrival']))))
		noise = [
			'BMOU4890244',
			'GATU8819107',
			'DFSU6029540',
			'EGSU9038678 EGSU9038678 EGSU9038678 EGSU9038678 EGSU9038678 EGSU9038678 EGSU9038678 EGSU9038678 EGSU9038678',
			'OOLU5680778 OOLU5735269'
		]
		for n in noise:
			arrival_ports.remove(n)
		arrival_ports_dict = { id: value for id, value in enumerate(arrival_ports) }
		arrival_ports_df = pd.DataFrame(list(arrival_ports_dict.items()), columns=['id', 'port_arrival'])
		arrival_ports_df.to_csv(arrival_ports_df_file, index=True)
	else:
		arrival_ports_df = pd.read_csv(arrival_ports_df_file,index_col=0)

	# --------------- #

	# Convert port_departure to ids [ 0 ... n-1]
	departure_ports_df_file = './../data/departure_port.csv'
	if os.path.isfile(departure_ports_df_file) == False:
		departure_ports = list(sorted(set(list(df['port_departure']))))
		noise = ['INBU5063680']
		for n in noise:
			departure_ports.remove(n)
		departure_ports_dict = { id: value for id, value in enumerate(departure_ports) }
		departure_ports_df = pd.DataFrame(list(departure_ports_dict.items()), columns=['id', 'port_departure'])
		departure_ports_df.to_csv(departure_ports_df_file, index=True)
	else:
		departure_ports_df = pd.read_csv(departure_ports_df_file, index_col=0)
	# --------------- #

	# Convert carrier to ids [0 ... n-1 ]
	carriers_df_file = './../data/carrier.csv'
	if os.path.isfile(carriers_df_file) == False:
		carriers = list(sorted(set(list(df['carrier']))))
		carriers_dict = { id: value for id, value in enumerate(carriers) }
		carriers_df = pd.DataFrame(list(carriers_dict.items()), columns=['id', 'carrier'])
		carriers_df.to_csv(carriers_df_file, index=True)
	else:
		carriers_df = pd.read_csv(carriers_df_file,index_col=0)

	# --------------- #

	src_cn_df_file = './../data/source_country.csv'
	if os.path.isfile(src_cn_df_file) == False:
		# Convert Source country to ids  [n-1]
		src_cn = list(set(list(df['source_country'])))
		src_cn = [s for s in src_cn if type(s) == str]
		src_cn = list(sorted(src_cn))
		src_cn_dict = { id: value for id, value in enumerate(src_cn) }
		src_cn_df = pd.DataFrame(list(src_cn_dict.items()), columns=['id', 'source_country'])
		src_cn_df.to_csv(src_cn_df_file, index=True)
	else :
		src_cn_df = pd.read_csv(src_cn_df_file,index_col=0)

	# --------------- #

	# arrival_ports_inv = { v: k for k, v in arrival_ports_dict.items() }
	# departure_ports_dict_inv = { v: k for k, v in departure_ports_dict.items() }
	# carriers_dict_inv = { v: k for k, v in carriers_dict.items() }
	# src_cn_dict_inv = { v: k for k, v in src_cn_dict.items() }
	rmv_list = []


	df = pd.merge(df, src_cn_df, suffixes=('','_'), on ='source_country')
	df = df.rename(columns = {'id': 'source_country_id'})
	del df['source_country']
	print(len(df))

	df = pd.merge(df, arrival_ports_df, on='port_arrival', suffixes=('','_'))
	df = df.rename(columns={ 'id': 'port_arrival_id' })
	del df['port_arrival']
	print(len(df))

	df =pd.merge(df , departure_ports_df,on='port_departure', suffixes=('','_'))
	df = df.rename(columns={ 'id': 'port_departure_id' })
	del df['port_departure']
	print(len(df))

	df = pd.merge(df, carriers_df,  on='carrier' , suffixes=('','_'))
	df = df.rename(columns={ 'id': 'carrier_id' })
	del df['carrier']

	print(df.columns)
	print(len(df))

	df = df.dropna(subset=['hs_code'])
	df.to_csv('../data/data_1.csv', index=True)

clean_data()
