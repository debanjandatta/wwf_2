import pandas as pd


def read_data():
				df = pd.read_csv('../data/data_1.csv',index_col=0)
				return df

def get_data_by_date(start_year = 2007, start_month= 1, end_year=2013,end_month = 12):
				df = read_data()
				df = df.loc[(((df['month']>= start_month) & (df['year'] >= start_year)) & ((df['month']<= end_month) & (df['year'] <= end_year)))]
				return df

df = get_data_by_date(2007,1,2007,6)
print(list(df.columns))
'''
Domains :
	hs_code
	port_arrival
	port_departure
	carrier
	source_country
'''

