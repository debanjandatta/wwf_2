import pandas as pd
import numpy as np
import sys
sys.path.append('./../..')
sys.path.append('./..')
import wwf_2.src.main_1 as data_aux


def fetch_data( ref_entity = 'source_country'):
	df = data_aux.get_data_by_date(start_year = 2007, start_month= 1, end_year=2007,end_month = 12)

	del df['port_lading']
	del df['port_discharge']
	print(df.head(5))

	print(df.columns)
	ref_attr = 'source_country_id'
	val_attr = 'weight'
	entity_attrs =  ['hs_code', 'source_country_id', 'port_arrival_id', 'port_departure_id', 'carrier_id']
	val_attr = 'weight'
	for e in entity_attrs :
		_df = df.groupby([ref_attr,e ])[val_attr].sum()
		_df = _df.to_frame()
		print(type)
		print(_df.head(10))
		print(_df.index)

fetch_data()